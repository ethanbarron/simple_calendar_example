class Listing < ApplicationRecord
  serialize :dates_blocked, Array

  has_many :reservations
  has_one :reservation_dates_blocked
end
