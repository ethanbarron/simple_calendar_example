Rails.application.routes.draw do
  resources :listings
  resources :reservations
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/listings/:id/toggle_date/:y/:m/:d', to: 'listings#toggle_date', as: 'listing_toggle_date'
end
