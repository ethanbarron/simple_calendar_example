class AddDatesBlockedToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :dates_blocked, :text
  end
end
