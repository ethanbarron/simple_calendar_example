# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

listing = Listing.create(
  name: 'My Listing',
  dates_blocked: [
    Date.new(2017, 3, 18),
    Date.new(2017, 3, 19),
    Date.new(2017, 3, 20),
    Date.new(2017, 3, 21),
    Date.new(2017, 4, 2),
    Date.new(2017, 4, 3),
    Date.new(2017, 4, 4),
    Date.new(2017, 4, 5)
  ]
)

Reservation.create(
  listing: listing,
  start_date: Date.new(2017, 3, 24),
  end_date: Date.new(2017, 3, 29)
)